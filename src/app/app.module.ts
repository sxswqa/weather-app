import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {WeatherForecastComponent} from './weather-forecast/weather-forecast.component';
import {HttpClientModule} from '@angular/common/http';
import {ApiService} from './services/api.service';
import {RouterModule, Routes} from '@angular/router';
import { CelsiusPipe } from './weather-forecast/celsius.pipe';
import { MomentPipe } from './weather-forecast/moment.pipe';
import { FullDatePipe } from './weather-forecast/full-date.pipe';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

@NgModule({
    declarations: [
        AppComponent,
        WeatherForecastComponent,
        CelsiusPipe,
        MomentPipe,
        FullDatePipe
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule
    ],
    providers: [ApiService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
