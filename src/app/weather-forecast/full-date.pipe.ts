import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'fullDate'
})
export class FullDatePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return moment.unix(value).format('DD MMMM YYYY').toLocaleLowerCase();
  }

}
