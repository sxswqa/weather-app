import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
    selector: 'app-weather-forecast',
    templateUrl: './weather-forecast.component.html',
    styleUrls: ['./weather-forecast.component.scss'],
    animations: [
        trigger('buttonState', [
            state('0', style({
                opacity: 1
            })),
            state('1', style({
                opacity: 1
            })),
            state('2', style({
                opacity: 1
            })),
            transition('0 => *', animate('500ms ease-out', style({
                transform: 'translateX(100%)',
                opacity: 0
            }))),
            transition('1 => 0', animate('500ms ease-in', style({
                transform: 'translateX(-100%)',
                opacity: 0
            }))),
            transition('1 => 2', animate('500ms ease-in', style({
                transform: 'translateX(100%)',
                opacity: 0
            }))),
            transition('2 => *', animate('500ms ease-in', style({
                transform: 'translateX(-100%)',
                opacity: 0
            }))),
        ])
    ]
})
export class WeatherForecastComponent implements OnInit {
    weather: any;
    objectKeys = Object.keys;
    selectedDate = [];
    imgMapping = new Map();
    state = '0';

    constructor(private apiService: ApiService) {
    }

    ngOnInit() {
        this.getKazanWeather();
        this.initImgMapping();
    }

    getKazanWeather() {
        this.apiService.getWeather().subscribe((res) => {
            this.weather = res;
            this.selectedDate.push(this.weather.list[0].weather[0].description);
            this.selectedDate.push(this.weather.list[0].temp.day);
            this.selectedDate.push(this.weather.list[0].dt);
            this.selectedDate.push(this.getImgUrl(0));
        });
    }

    onSelect(key) {
        this.selectedDate = [];
        this.selectedDate.push(this.weather.list[key].weather[0].description);
        this.selectedDate.push(this.weather.list[key].temp.day);
        this.selectedDate.push(this.weather.list[key].dt);
        this.selectedDate.push(this.getImgUrl(key));
    }

    getImgUrl(key) {
        return this.imgMapping.get(this.weather.list[key].weather[0].main);
    }

    initImgMapping() {
        this.imgMapping.set('Clouds', '/assets/img/clouds.svg');
        this.imgMapping.set('Clear', '/assets/img/clear_sky.svg');
        this.imgMapping.set('Rain', '/assets/img/light_rain.svg');
    }

    toggleState(key) {
        this.state = String(key);
    }
}
